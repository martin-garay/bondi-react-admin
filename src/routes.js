import * as React from "react";
import { ColorField, ColorInput } from 'react-admin-color-input';
import {
  Datagrid,
  List,
  Show,
  Create,
  Edit,
  Filter,
  SimpleShowLayout,
  TextField,
  TextInput,
  ShowButton,
  EditButton,
  DeleteButton,    
  TabbedForm,
  FormTab,
  ArrayInput,
  SimpleFormIterator,  
  FormDataConsumer

} from "react-admin";
import MapInput from "./MapListComponent/MapInput";

const RouteFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="title" alwaysOn />
  </Filter>
);

export const RouteList = props => (
  <List
    {...props}
    filters={<RouteFilter />}
    //filter={{ updatedby: "test@example.com" }}
  >
    <Datagrid>
      {/* <TextField source="id" /> */}
      <TextField source="name" />
      <ColorField source="color" />      
      <ShowButton label="" />
      <EditButton label="" />
      <DeleteButton label="" redirect={false} />
    </Datagrid>
  </List>
);

export const RouteShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
        <TextField source="id" />        
        <TextField source="name" />      
        <TextField source="recorrido" />      
        <ColorField source="color" />
    </SimpleShowLayout>
  </Show>
);

export const RouteCreate = props => (
  <Create {...props}>
    <TabbedForm>
        <FormTab label="Datos">            
            <TextInput source="id" validation={{ required: true }}/>
            <TextInput source="name" validation={{ required: true }}/>
            <TextInput source="recorrido" options={{ multiline: true }}/>
            <ColorInput source="color" validation={{ required: true }}/>
        </FormTab>
        <FormTab label="Mapa">             
            <FormDataConsumer>
              {({record}) => {
                  return (
                    <MapInput               
                      source="coordinates"
                      zoom={13}
                      defaultCenter={{ lat: -34.566796, lng: -59.115170}}
                      colorLine={record.color}
                    />
                  );
              }}
            </FormDataConsumer>                                  
        </FormTab>
        <FormTab label="Horarios">
          <ArrayInput source="horarios">
            <SimpleFormIterator disableRemove>              
                <TextInput source="show" label=""/>                
                <ArrayInput source="horas" label="">
                  <SimpleFormIterator>
                    <FormDataConsumer>
                      {({id}) => {
                          return (
                            <TextInput source={id} label=""/>
                          );
                      }}
                  </FormDataConsumer>                      
                  </SimpleFormIterator>
              </ArrayInput>
            </SimpleFormIterator>
            </ArrayInput>
        </FormTab>
    </TabbedForm>
  </Create>
);
export const RouteEdit = props => (
  
  <Edit undoable={true} {...props}>        
    <TabbedForm>
        <FormTab label="Datos">            
            <TextInput disabled source="id" validation={{ required: true }}/>
            <TextInput source="name" validation={{ required: true }}/>
            <TextInput source="recorrido" options={{ multiline: true }}/>
            <ColorInput source="color" validation={{ required: true }}/>
        </FormTab>
        <FormTab label="Mapa">             
            <FormDataConsumer>
              {({record}) => {
                  return (
                    <MapInput               
                      source="coordinates"
                      zoom={13}
                      defaultCenter={{ lat: -34.566796, lng: -59.115170}}
                      colorLine={record.color}
                    />
                  );
              }}
            </FormDataConsumer>                                  
        </FormTab>
        <FormTab label="Horarios">
          <ArrayInput source="horarios">
            <SimpleFormIterator disableRemove>              
                <TextInput source="show" label=""/>                
                <ArrayInput source="horas" label="">
                  <SimpleFormIterator>
                    <FormDataConsumer>
                      {({id}) => {
                          return (
                            <TextInput source={id} label=""/>
                          );
                      }}
                  </FormDataConsumer>                      
                  </SimpleFormIterator>
              </ArrayInput>
            </SimpleFormIterator>
            </ArrayInput>
        </FormTab>
    </TabbedForm>
  </Edit>
);

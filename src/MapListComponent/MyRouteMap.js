import React from 'react';
import { Map, GoogleApiWrapper, Marker, Polyline } from 'google-maps-react';
import PropTypes from 'prop-types';

class MyRouteMap extends React.Component {
    static defaultProps = {
      zoom: 13,
      colorLine: '#00ffff',
      coordinates:[]      
    }

    handleOnMoveMarker = (index,newPosition) => {
      this.props.onMoveMarker(index,newPosition);
    }
    handleOnSelectMarker = (index) => {
      this.props.onSelectMarker(index);
    }
    handleOnMarkerDragEnd = ({ latLng }, index) => {      
      let lat = latLng.lat();
      let lng = latLng.lng();
      let newPosition = {lat , lng};
      this.handleOnMoveMarker(index,newPosition);
    }
    
    renderMarkers = () => {
      return this.props.coordinates.map(({lat,lng}, index) => {
        const isSelected = index === this.props.selected;
        return <Marker
                    key={index} 
                    id={index}
                    name={index}
                    draggable={true}
                    animation={isSelected ? this.props.google.maps.Animation.BOUNCE : null}                         
                    position={{ lat, lng }}
                    onClick={() => this.handleOnSelectMarker(index)} 
                    onDragend={(t, map, coord) => this.handleOnMarkerDragEnd(coord, index)}
                />
      })
    }

    render() {     
      
      return (
        <div style={mapStyles}>
          <Map
            google={this.props.google}
            zoom={this.props.zoom}
            center={this.props.center}
            style={mapStyles}
            zoomControl={true}
            initialCenter={this.props.center}
            scrollwheel={true}
            onClick={this.props.onClickMap}
          >
            
            {(this.props.showMarkers && this.props.coordinates) ? this.renderMarkers() : null}
            {this.props.coordinates ?             
              <Polyline 
                  path={this.props.coordinates}              
                  options={{ 
                      strokeColor: this.props.colorLine,
                      strokeOpacity: 1,
                      strokeWeight: 2,
                      icons: [{ 
                          icon: {path: this.props.google.maps.SymbolPath.FORWARD_CLOSED_ARROW},
                          offset: '0',
                          repeat: '150px',                        
                      }],
                  }}
              />
            : null}
          </Map>
          </div>
      );
    }
}

const mapStyles = {
    position: 'relative',    
    width: '100%',
    height: '60vh',
};

MyRouteMap.propTypes = {
    coordinates: PropTypes.array,
    selected: PropTypes.number,
    showMarkers: PropTypes.bool.isRequired,
    onClickMap: PropTypes.func,
    onSelectMarker: PropTypes.func.isRequired,
    onMoveMarker: PropTypes.func.isRequired,
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyChiPmjq-hr2QNpsWyhOxnv2i0anEyzN2U"
})(MyRouteMap);  
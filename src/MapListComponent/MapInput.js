import React, { Component } from 'react';
import { addField } from 'ra-core';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { MyMapList } from './MyMapList';

class MapComponent extends Component {
  static defaultProps = {
    coordinates:[]      
  }
  constructor(props){
    super(props);
    const { defaultCenter } = this.props;

    this.state = {
      selected: this.props.selected ? this.props.selected : null,
      center: defaultCenter || { lat: 0, lng: 0 },
      showMarkers: true
    }
  }

  getMarkers = input => {
      return input.value || [];
  }  
  getPosition = e => ({    
    lat: e.latLng.lat(),
    lng: e.latLng.lng(),
  });
  handleOnSelectMarker = ({index,input}) => {
    let coordinates = this.getMarkers(input)
    this.setState({
      selected:index,
      center: coordinates[index]
    })
  }
  handleOnMoveMarker = ({index,newPosition,input}) => {            
      let coordinates = [...this.getMarkers(input)];            
      coordinates[index] = {...newPosition};
      input.onChange(coordinates);      
  }
  handleOnDeleteMarker = (index,input) => {
    let coordinates = [...this.getMarkers(input)];
    coordinates.splice(index, 1);
    input.onChange(coordinates);
  }
  setCenter = markerPos => this.setState({ center: markerPos });

  putMarker = ({ markerPos, input }) => {
    const currentValue = this.getMarkers(input);
    if (currentValue && currentValue !== null) {      
      this.setCenter(markerPos);

      return input.onChange([...currentValue, markerPos]);
          
    }    
  }
  handleToogleShowMarkers = () => {    
    this.setState({showMarkers: !this.state.showMarkers});
  }

  render() {    
    const { input } = this.props;
    return (
        <div>        
          <FormControlLabel
            control={
              <Switch
                checked={this.state.showMarkers}
                onChange={this.handleToogleShowMarkers}
                color="primary"
                name="Marcadores"
                inputProps={{ 'aria-label': 'primary checkbox' }}
              />      
            }
            label="Marcadores"
          />    

          <MyMapList
            coordinates={this.getMarkers(input)}
            selected={this.state.selected}
            center={this.state.center}
            zoom={this.props.zoom}
            showMarkers={this.state.showMarkers}
            colorLine={this.props.colorLine}
            onSelectMarker={(index) => this.handleOnSelectMarker({
              index,
              input
            })}
            onMoveMarker={(index,newPosition)=>this.handleOnMoveMarker({
              index,
              newPosition,
              input
            })}            
            onClickMap={(e,e2,e3) => this.putMarker({
                input,
                markerPos: this.getPosition(e3)
            })}
            onDeleteMarker={(index)=>this.handleOnDeleteMarker(index,input)}  
            onInsertMarker={()=>null}                                      
          />
        </div>
    );
  }
}

const MapInput = addField(MapComponent);
export default MapInput;

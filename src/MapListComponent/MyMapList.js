import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import MyRouteMap from './MyRouteMap';
import ListMarkers from './ListMarkers';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

export const MyMapList = (props) =>{
    const classes = useStyles();

    return (      
          <Grid container spacing={3}>            
            <Grid item xs={10}>
              <Paper className={classes.paper}>
                  <MyRouteMap 
                      coordinates={props.coordinates}
                      selected={props.selected}
                      onClickMap={props.onClickMap}
                      onSelectMarker={props.onSelectMarker}
                      onMoveMarker={props.onMoveMarker}
                      center={props.center}
                      showMarkers={props.showMarkers}
                      colorLine={props.colorLine}
                  />
              </Paper>
            </Grid>
            <Grid item xs={2}>
              <Paper className={classes.paper}>
                  <ListMarkers
                    coordinates={props.coordinates}
                    selected={props.selected}                    
                    onSelectMarker={props.onSelectMarker}
                    onDeleteMarker={props.onDeleteMarker}
                    onInsertMarker={props.onInsertMarker}
                  />      
              </Paper>
            </Grid>
          </Grid>
      );
}

MyMapList.propTypes = {
  coordinates: PropTypes.array,  
  selected: PropTypes.number,
  showMarkers: PropTypes.bool.isRequired,
  onClickMap: PropTypes.func,
  onSelectMarker: PropTypes.func.isRequired,
  onDeleteMarker: PropTypes.func.isRequired,
  onInsertMarker: PropTypes.func.isRequired,
  onMoveMarker: PropTypes.func.isRequired,
};


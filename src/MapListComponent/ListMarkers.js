import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import RoomIcon from '@material-ui/icons/Room';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    overflow: 'auto',
    maxHeight: 350,    
  },
}));


export default function ListMarkers(props) {
  const classes = useStyles();
  
  const drawItemsList = () => {
    return props.coordinates.map((item,index) =>               
        <ListItem
            key={index}
            button
            selected={props.selected === index}
            onClick={() => props.onSelectMarker(index)}
        >            
            {index+1}
            <ListItemIcon>                
                <RoomIcon />                
            </ListItemIcon>            
            <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="delete" onClick={() => props.onDeleteMarker(index)}>
                    <DeleteIcon />
                </IconButton>                        
            </ListItemSecondaryAction>
        </ListItem>        
    )
  }

  return (
    <div className={classes.root}>
      <List component="nav" aria-label="Marcadores">      
        {props.coordinates && drawItemsList()}
      </List>            
    </div>
  );
}

ListMarkers.propTypes = {
    coordinates: PropTypes.array,
    selected: PropTypes.number,
    onSelectMarker: PropTypes.func.isRequired,
    onDeleteMarker: PropTypes.func.isRequired,
    onInsertMarker: PropTypes.func.isRequired,
}

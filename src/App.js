import * as React from "react";
import { UserList, UserShow, UserCreate, UserEdit } from "./users";
import { RouteList, RouteShow, RouteCreate, RouteEdit } from "./routes";
import { Admin, Resource } from "react-admin";
import {
  FirebaseDataProvider,
  FirebaseAuthProvider
} from "react-admin-firebase";
import firebase from 'firebase';
import UserIcon from '@material-ui/icons/People';

import { firebaseConfig } from './FIREBASE_CONFIG';
import CustomLoginPage from './CustomLoginPage';

const firebaseApp = firebase.initializeApp(firebaseConfig);

const options = {
  logging: false,
  //rootRef: 'BussRoutes/Tyne8cOXLYDmjpC0BanP',
  app: firebaseApp,
  // watch: ['posts'];
  // dontwatch: ['BussRoutes'];
  persistence: 'local',
  // disableMeta: true
  dontAddIdFieldToDoc: true
}

const authProvider = FirebaseAuthProvider(firebaseConfig, options);
const dataProvider = FirebaseDataProvider(firebaseConfig, options);

class App extends React.Component {
  render() {
    return (
      <Admin
        loginPage={CustomLoginPage}
        dataProvider={dataProvider}
        authProvider={authProvider}
      >        
        <Resource
          name="BussRoutes"
          list={RouteList}
          show={RouteShow}
          create={RouteCreate}
          edit={RouteEdit}
        />
        <Resource
          name="users"
          icon={UserIcon}
          list={UserList}
          show={UserShow}
          create={UserCreate}
          edit={UserEdit}
        />
      </Admin>
    );
  }
}

export default App;
